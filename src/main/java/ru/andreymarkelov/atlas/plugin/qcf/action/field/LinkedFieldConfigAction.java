package ru.andreymarkelov.atlas.plugin.qcf.action.field;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import ru.andreymarkelov.atlas.plugin.qcf.manager.LinkedFieldConfigManager;
import ru.andreymarkelov.atlas.plugin.qcf.model.LinkedFieldData;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LinkedFieldConfigAction extends BaseQueryFieldConfigAction {
    private final LinkedFieldConfigManager linkedFieldConfigManager;

    private String jql;
    private String[] displayFields;

    public LinkedFieldConfigAction(
            ManagedConfigurationItemService managedConfigurationItemService,
            AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            LinkedFieldConfigManager linkedFieldConfigManager) {
        super(managedConfigurationItemService, autoCompleteJsonGenerator);
        this.linkedFieldConfigManager = linkedFieldConfigManager;
    }

    @Override
    public String doDefault() throws Exception {
        if (!hasGlobalPermission(GlobalPermissionKey.ADMINISTER)) {
            return "securitybreach";
        }

        LinkedFieldData linkedFieldData = linkedFieldConfigManager.get(getFieldConfig());
        if (linkedFieldData != null) {
            jql = linkedFieldData.getJql();
            displayFields = linkedFieldData.getDisplayFields();
        }
        return INPUT;
    }

    @Override
    @RequiresXsrfCheck
    public String doExecute() throws Exception {
        if (!hasGlobalPermission(GlobalPermissionKey.ADMINISTER)) {
            return "securitybreach";
        }

        linkedFieldConfigManager.set(getFieldConfig(), new LinkedFieldData(jql, displayFields));
        return getRedirect("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + getFieldConfig().getCustomField().getIdAsLong().toString());
    }

    public Map<String, String> getAllOptions() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("key", "queryfields.opt.key");
        map.put("status", "queryfields.opt.status");
        map.put("assignee", "queryfields.opt.assignee");
        map.put("due", "queryfields.opt.due");
        map.put("priority", "queryfields.opt.priority");
        return map;
    }

    public String getJql() {
        return jql;
    }

    public void setJql(String jql) {
        this.jql = jql;
    }

    public List<String> getDisplayFields() {
        return Arrays.asList(displayFields);
    }

    public void setDisplayFields(String[] displayFields) {
        this.displayFields = displayFields;
    }
}
