package ru.andreymarkelov.atlas.plugin.qcf.manager;

import com.atlassian.jira.issue.fields.config.FieldConfig;
import ru.andreymarkelov.atlas.plugin.qcf.model.LinkedFieldData;

public interface LinkedFieldConfigManager {
    LinkedFieldData get(FieldConfig fieldConfig);
    void set(FieldConfig fieldConfig, LinkedFieldData linkedFieldData);
}
