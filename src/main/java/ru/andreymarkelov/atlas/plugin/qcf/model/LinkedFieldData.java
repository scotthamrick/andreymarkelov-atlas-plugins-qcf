package ru.andreymarkelov.atlas.plugin.qcf.model;

import java.util.Arrays;

public class LinkedFieldData {
    private String jql;
    private String[] displayFields;

    public LinkedFieldData(String jql, String[] displayFields) {
        this.jql = jql;
        this.displayFields = displayFields;
    }

    public String getJql() {
        return jql;
    }

    public void setJql(String jql) {
        this.jql = jql;
    }

    public String[] getDisplayFields() {
        return displayFields;
    }

    public void setDisplayFields(String[] displayFields) {
        this.displayFields = displayFields;
    }

    @Override
    public String toString() {
        return "LinkedFieldData{" +
                "jql='" + jql + '\'' +
                ", displayFields=" + Arrays.toString(displayFields) +
                '}';
    }
}
