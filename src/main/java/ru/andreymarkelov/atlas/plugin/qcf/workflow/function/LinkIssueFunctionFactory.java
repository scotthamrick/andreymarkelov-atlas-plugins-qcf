package ru.andreymarkelov.atlas.plugin.qcf.workflow.function;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import org.apache.commons.lang3.StringUtils;
import ru.andreymarkelov.atlas.plugin.qcf.field.LinkerMultiField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LinkIssueFunctionFactory extends BaseFunctionFactory {
    private final IssueLinkTypeManager issueLinkTypeManager;

    public LinkIssueFunctionFactory(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext,
            IssueLinkTypeManager issueLinkTypeManager) {
        super(customFieldManager, jiraAuthenticationContext);
        this.issueLinkTypeManager=issueLinkTypeManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put("customFields", getLinkerMultiFields());
        velocityParams.put("linkTypes", getIssueLinkTypes());
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("customFields", getLinkerMultiFields());
        velocityParams.put("linkTypes", getIssueLinkTypes());
        velocityParams.put("multiLinkerField", getParam(descriptor, "multiLinkerField"));
        velocityParams.put("linkType", getParam(descriptor, "linkType"));
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put("multiLinkerField", getParam(descriptor, "multiLinkerField"));
        velocityParams.put("linkType", getLinkTypeName(getParam(descriptor, "linkType")));
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> functionParams) {
        Map<String, Object> descriptorParams = new HashMap<String, Object>();

        if (functionParams != null && functionParams.containsKey("multiLinkerField")) {
            descriptorParams.put("multiLinkerField", extractSingleParam(functionParams, "multiLinkerField"));
        } else {
            descriptorParams.put("multiLinkerField", "");
        }
        if (functionParams != null && functionParams.containsKey("linkType")) {
            descriptorParams.put("linkType", extractSingleParam(functionParams, "linkType"));
        } else {
            descriptorParams.put("linkType", "");
        }

        return descriptorParams;
    }

    private Map<Long, String> getIssueLinkTypes() {
        Map<Long, String> map = new TreeMap<Long, String>();
        for (IssueLinkType issueLinkType : issueLinkTypeManager.getIssueLinkTypes()) {
            map.put(issueLinkType.getId(), issueLinkType.getName());
        }
        return map;
    }

    private String getLinkTypeName(String linkTypeId) {
        return issueLinkTypeManager.getIssueLinkType(Long.valueOf(linkTypeId)).getName();
    }
}

