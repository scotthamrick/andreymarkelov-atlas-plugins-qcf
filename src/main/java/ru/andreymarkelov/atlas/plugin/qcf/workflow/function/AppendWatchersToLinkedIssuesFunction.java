package ru.andreymarkelov.atlas.plugin.qcf.workflow.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

public class AppendWatchersToLinkedIssuesFunction extends AbstractJiraFunctionProvider {
    private final static Logger log = Logger.getLogger(AppendWatchersToLinkedIssuesFunction.class);

    private final WatcherManager watcherManager;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final CustomFieldManager customFieldManager;



    public AppendWatchersToLinkedIssuesFunction(
            WatcherManager watcherManager,
            IssueManager issueManager,
            JiraAuthenticationContext jiraAuthenticationContext,
            CustomFieldManager customFieldManager) {
        this.watcherManager = watcherManager;
        this.issueManager = issueManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public void execute(
            @SuppressWarnings("rawtypes") Map transientVars,
            @SuppressWarnings("rawtypes") Map args,
            PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);
        String assignee = (String) args.get("assignee");
        String reporter = (String) args.get("reporter");
        String multiLinkerField = (String) args.get("multiLinkerField");

        CustomField customField = customFieldManager.getCustomFieldObjectByName(multiLinkerField);
        if (customField == null) {
            log.error("Link Issue Function: Multi Linker custom field is not set");
            throw new WorkflowException(jiraAuthenticationContext.getI18nHelper().getText("queryfields.postfunction.linkissues.error"));
        }

        List<String> issuesKeys = (List<String>) issue.getCustomFieldValue(customField);
        if (issuesKeys == null) {
            return;
        }

        if (!StringUtils.isBlank(assignee)) {
            ApplicationUser assigneeUser = issue.getAssigneeUser();
            if (assigneeUser != null) {
                addUserToIssuesWatchers(assigneeUser, getIssuesFromKeys(issuesKeys));
            }
        }

        if (!StringUtils.isBlank(reporter)) {
            ApplicationUser reporterUser = issue.getReporterUser();
            if (reporterUser != null) {
                addUserToIssuesWatchers(reporterUser, getIssuesFromKeys(issuesKeys));
            }
        }
    }

    private void addUserToIssueWatchers(ApplicationUser applicationUser, Issue issue) {
        if (!watcherManager.isWatching(applicationUser, issue)) {
            watcherManager.startWatching(applicationUser, issue);
        }
    }

    private void addUserToIssuesWatchers(ApplicationUser applicationUser, Collection<Issue> issues) {
        for (Issue issue : issues) {
            addUserToIssueWatchers(applicationUser, issue);
        }
    }

    private List<Issue> getIssuesFromKeys(List<String> issuesKeys) {
        List<Issue> issues = new ArrayList<Issue>();
        for (String issuesKey : issuesKeys) {
            Issue issue = issueManager.getIssueObject(issuesKey);
            if (issue != null) {
                issues.add(issue);
            }
        }
        return issues;
    }
}

