package ru.andreymarkelov.atlas.plugin.qcf.field.config;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import ru.andreymarkelov.atlas.plugin.qcf.manager.LinkerFieldConfigManager;

public class LinkerFieldConfig implements FieldConfigItemType {
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final LinkerFieldConfigManager linkerFieldConfigManager;

    public LinkerFieldConfig(
            JiraAuthenticationContext jiraAuthenticationContext,
            LinkerFieldConfigManager linkerFieldConfigManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.linkerFieldConfigManager = linkerFieldConfigManager;
    }

    @Override
    public String getDisplayName() {
        return jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugin.qcf.field.linkerfield.config.edit");
    }

    @Override
    public String getDisplayNameKey() {
        return "ru.andreymarkelov.atlas.plugin.qcf.field.linkerfield.config.edit";
    }

    @Override
    public String getViewHtml(FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem) {
        return null;
    }

    @Override
    public String getObjectKey() {
        return "LinkerFieldConfigAction";
    }

    @Override
    public Object getConfigurationObject(Issue issue, FieldConfig fieldConfig) {
        return null;
    }

    @Override
    public String getBaseEditUrl() {
        return "LinkerFieldConfigAction!default.jspa";
    }
}
